<?php
$msg = "TEST";
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;
$msg .= $msg;

$msg=base64_encode($msg);

$img_in = "in.png";
$img_out = "out.png";
$offX = 73;
$width = 73;
$offY = 73;

commit ( $img_in, $img_out, $msg, $offX, $width, $offY );

echo "Done.\n";

function commit($img_in, $img_out, $msg, $offX, $width, $offY) {
	$width*=8;
	$img = imagecreatefrompng ( $img_in );
	if (! $img) {
		die ( "No input image.\n" );
	}
	$i = 0;
	$len = strlen ( $msg );
	
	for($y = $offY;; $y ++) {
		
		for($x = $offX; $x < $offX + $width; $x += 8) {
			
			if ($i == $len) {
				imagepng ( $img, $img_out );
				imagedestroy ( $img );
				return;
			}
			draw ( $img, $x, $y, substr ( $msg, $i, 1 ) );
			$i ++;
		}
	}
}

function draw($img, $bX, $bY, $Char) {
	echo "Drawing " . $Char . " to " . $bX . ":" . $bY . "\n";
	$c = ord ( $Char );
	$mask = 0;
	echo "Drawing $Char to $bX, $bY\n";
	$b1 = ($c & 0b10000000) > 0 ? setPix ( $img, $bX + 0, $bY, $mask ) : 0;
	$b2 = ($c & 0b01000000) > 0 ? setPix ( $img, $bX + 1, $bY, $mask ) : 0;
	$b3 = ($c & 0b00100000) > 0 ? setPix ( $img, $bX + 2, $bY, $mask ) : 0;
	$b4 = ($c & 0b00010000) > 0 ? setPix ( $img, $bX + 3, $bY, $mask ) : 0;
	$b5 = ($c & 0b00001000) > 0 ? setPix ( $img, $bX + 4, $bY, $mask ) : 0;
	$b6 = ($c & 0b00000100) > 0 ? setPix ( $img, $bX + 5, $bY, $mask ) : 0;
	$b7 = ($c & 0b00000010) > 0 ? setPix ( $img, $bX + 6, $bY, $mask ) : 0;
	$b8 = ($c & 0b00000001) > 0 ? setPix ( $img, $bX + 7, $bY, $mask ) : 0;
}

function setPix($img, $x, $y, $mask) {
	$pix = imagecolorat ( $img, $x, $y );
	$c = array ();
	$c [] = ($pix >> 16) & 0xff;
	$c [] = ($pix >> 8) & 0xff;
	$c [] = ($pix >> 0) & 0xff;
	if ($c [$mask] > 127) {
		$c [$mask] -= 1;
	} else {
		$c [$mask] += 1;
	}
	
	$pix = ($c [0] << 16) | ($c [1] << 8) | ($c [2] << 0);
	imagesetpixel ( $img, $x, $y, $pix );
}

?>