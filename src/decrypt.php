<?php
decrypt_auto ( "in.png", "out.png", "legacyArts" );

echo "Done";

function decrypt_auto($in1, $in2, $outPrefix) {
	echo "Running automatic spectral analysis for channels 0,1 and 2\n";
	spectralize ( $in1, $in2, $outPrefix . "_spect_R.png", 0 );
	spectralize ( $in1, $in2, $outPrefix . "_spect_G.png", 1 );
	spectralize ( $in1, $in2, $outPrefix . "_spect_B.png", 2 );
	echo "Running decoding spectral data process\n";
	decode ( $outPrefix . "_spect_R.png", $outPrefix . "_data_R.txt" );
	decode ( $outPrefix . "_spect_G.png", $outPrefix . "_data_G.txt" );
	decode ( $outPrefix . "_spect_B.png", $outPrefix . "_data_B.txt" );
}

function decrypt($in1, $in2, $spect, $channel, $out) {
	spectralize ( $in1, $in2, $spect, $channel );
	decode ( $spect, $out );
}

function decode($img, $out) {
	$img = imagecreatefrompng ( $img );
	$width = imagesx ( $img );
	$height = imagesy ( $img );
	echo "Searching chunk borders...\n";
	$g = false;
	for($y = 0; $y < $height; $y ++) {
		for($x = 0; $x < $width; $x ++) {
			$pix = imagecolorat ( $img, $x, $y );
			if ($pix == 0) {
				$g = true;
				break 2;
			}
		}
	}
	if (! $g) {
		echo "No data present.\n";
		return;
	}
	$top = $y;
	for($y = $top; $y < $height; $y ++) {
		$f = false;
		for($x = 0; $x < $width; $x ++) {
			$pix = imagecolorat ( $img, $x, $y );
			if ($pix == 0) {
				$f = true;
				break;
			}
		}
		if (! $f) {
			break;
		}
	}
	$bottom = $y - 1;
	$l = $width;
	for($y = $top, $ym = $bottom; $y < $ym; $y ++) {
		for($x = 0; $x < $width; $x ++) {
			$pix = imagecolorat ( $img, $x, $y );
			if ($pix == 0) {
				if ($x < $l) {
					$l = $x;
				}
				break;
			}
		}
	}
	$left = $l;
	$l = 0;
	for($y = $top, $ym = $bottom; $y < $ym; $y ++) {
		for($x = $width - 1; $x >= $left; $x --) {
			$pix = imagecolorat ( $img, $x, $y );
			if ($pix == 0) {
				if ($x > $l) {
					$l = $x;
				}
				break;
			}
		}
	}
	$right = $l;
	$lines = $bottom - $top;
	$str = "";
	$left --;
	if ($right == 0 || $left == $width || $top > $bottom || $left >= $right) {
		echo "No data found\n";
		return;
	}
	$data = ceil ( (($right - $left) * ($bottom - $top)) / 8 );
	echo "Cumulating $data bytes of potentional data. Decryption process initiated.\n";
	try {
		for($y = $top, $ym = $bottom; $y < $ym; $y ++) {
			for($x = $left, $xm = $right; $x < $xm; $x += 8) {
				$str .= readChar ( $img, $x, $y );
			}
		}
		imagedestroy ( $img );
		$str = trim ( $str );
		file_put_contents ( $out, $str );
		echo "Saving " . strlen ( $str ) . " bytes of data.\n";
	} catch ( Exception $e ) {
		echo "Error decrypting data. Possibly no data present in current spectre.\n";
		return;
	}
}

function readChar($image, $x, $y) {
	$b1 = imagecolorat ( $image, $x + 0, $y ) == 0 ? 1 : 0;
	$b2 = imagecolorat ( $image, $x + 1, $y ) == 0 ? 1 : 0;
	$b3 = imagecolorat ( $image, $x + 2, $y ) == 0 ? 1 : 0;
	$b4 = imagecolorat ( $image, $x + 3, $y ) == 0 ? 1 : 0;
	$b5 = imagecolorat ( $image, $x + 4, $y ) == 0 ? 1 : 0;
	$b6 = imagecolorat ( $image, $x + 5, $y ) == 0 ? 1 : 0;
	$b7 = imagecolorat ( $image, $x + 6, $y ) == 0 ? 1 : 0;
	$b8 = imagecolorat ( $image, $x + 7, $y ) == 0 ? 1 : 0;
	$d = ($b1 << 7) | ($b2 << 6) | ($b3 << 5) | ($b4 << 4) | ($b5 << 3) | ($b6 << 2) | ($b7 << 1) | ($b8 << 0);
	return chr ( $d );
}

function spectralize($img1, $img2, $img_out, $channel) {
	$channelDescr = $channel;
	$channel *= 8;
	copy ( $img2, $img_out );
	$img1 = imagecreatefrompng ( $img1 );
	$img2 = imagecreatefrompng ( $img2 );
	$width = imagesx ( $img1 );
	$height = imagesy ( $img1 );
	$out = imagecreatefrompng ( $img_out );
	echo "Running spectral analysis for channel $channelDescr ...\n";
	for($y = 0; $y < $height; $y ++) {
		for($x = 0; $x < $width; $x ++) {
			$pix1 = imagecolorat ( $img1, $x, $y );
			$pix2 = imagecolorat ( $img2, $x, $y );
			$pix1 = ($pix1 >> $channel) & 0xff;
			$pix2 = ($pix2 >> $channel) & 0xff;
			if ($pix1 != $pix2) {
				imagesetpixel ( $out, $x, $y, 0x000000 );
			} else {
				imagesetpixel ( $out, $x, $y, 0xffffff );
			}
		}
	}
	imagepng ( $out, $img_out );
	imagedestroy ( $out );
	imagedestroy ( $img1 );
	imagedestroy ( $img2 );
}

?>